// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyD43_ePa80cyQdknw9K_lMgkNQiyD0cVNE",
    authDomain: "nodeapp-7930e.firebaseapp.com",
    databaseURL: "https://nodeapp-7930e.firebaseio.com",
    projectId: "nodeapp-7930e",
    storageBucket: "nodeapp-7930e.appspot.com",
    messagingSenderId: "660264988885",
    appId: "1:660264988885:web:be3e0ebaeb46789ec3725c",
    measurementId: "G-YED3ZQV020"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
