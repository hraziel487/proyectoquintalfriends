import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes, CanActivate } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },  
  {
    path: 'login',
    loadChildren: () => import('./componentes/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./componentes/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./componentes/home/home.module').then( m => m.HomePageModule), canActivate: [AuthGuard]
  },
  {
    path: 'galeria',
    loadChildren: () => import('./componentes/galeria/galeria.module').then( m => m.GaleriaPageModule)
  },
  {
    path: 'imagen-modal',
    loadChildren: () => import('./componentes/imagen-modal/imagen-modal.module').then( m => m.ImagenModalPageModule)
  },
  {
    path: 'blog',
    loadChildren: () => import('./componentes/blog/blog.module').then( m => m.BlogPageModule)
  },
  {
    path: 'editar-blog/:id',
    loadChildren: () => import('./componentes/editar-blog/editar-blog.module').then( m => m.EditarBlogPageModule)
  },
  {
    path: 'crear-blog',
    loadChildren: () => import('./componentes/crear-blog/crear-blog.module').then( m => m.CrearBlogPageModule)
  },
  {
    path: 'admin',
    loadChildren: () => import('./componentes/admin/admin.module').then( m => m.AdminPageModule), canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
