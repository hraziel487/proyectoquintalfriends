import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditarBlogPageRoutingModule } from './editar-blog-routing.module';

import { EditarBlogPage } from './editar-blog.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    EditarBlogPageRoutingModule
  ],
  declarations: [EditarBlogPage]
})
export class EditarBlogPageModule {}
