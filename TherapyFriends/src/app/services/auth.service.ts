import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { async } from '@angular/core/testing';
import { User } from '../componentes/shared/user.class';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
public isLogged: any = false;

  constructor(public afAuth: AngularFireAuth) {
    afAuth.authState.subscribe(user =>(this.isLogged = user));
  }
//Login
async onLogin(user: User){
  try{
    return await this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password);
  } catch (error){

    console.log('error en Login', error);
  }
 }
//register
async onRegister(user: User){
  try{
    return await this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
  } catch (error){
    console.log('error en Register', error);
    }
  }
}
